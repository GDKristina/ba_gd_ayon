﻿# This script is for defining special effects that appear in the game.

###############



####### This is for the main menu image map buttons
transform buttonfade:

    on idle:
        alpha 1.0
    on hover:
            alpha 0.0
            linear 0.1 alpha 1.0
    on selected_hover:
            alpha 0.0
            linear 0.1 alpha 1.0





####### This is to generate some random special character strings
####### Use these for some extra spook during intense scenes

init python:
    import random

    nonunicode = "^°!§$%&/()=?`´+*~#;-<>|¡¢£¤¥¦§¨©ª«¬®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲĳĴĵĶķĸĹĺĻļĽľĿŀŁłŃńŅņŇňŉŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽž"

    def spooktext(length):
        output = ""
        for x in range(length):
            output += random.choice(nonunicode)
        return output


###### This is supposed to make the "anxiety text", ie slightly random text size

style anxiety_text:
    size  random.randint(20, 40)
    color "#ffffff"
    font "DejaVuSans.ttf"

    ##### show text "{=anxiety_text}That's one way to solve the problem.{/=anxiety_text}" shows a small text INSIDE the scene for a split second.
    ##### Not the intended effect but still kind of fun?



######## This is for taking screenshots I guess?
# Define some python stuff
init python:
    # Screen cap the current screen, used by multiple functions
    def screenshot_srf():
        srf = renpy.display.draw.screenshot(None, False)
        # if srf.get_width != 1280: srf = renpy.display.scale.smoothscale(srf, (1280, 720))
        return srf




    #Hide all windows, revealing the background
    def hide_windows_enabled(enabled=True):
        global _windows_hidden
        _windows_hidden = not enabled




########## All of this is for making real time glitches in the screen. Discard this if it doesn't work
########## This is from the DDLC Modding files.
init python:
    #This class defines the little blinking pieces of the screen tear effect
    class TearPiece:
        def __init__(self, startY, endY, offtimeMult, ontimeMult, offsetMin, offsetMax):
            self.startY = startY
            self.endY = endY
            self.offTime = (random.random() * 0.2 + 0.2) * offtimeMult
            self.onTime = (random.random() * 0.2 + 0.2) * ontimeMult
            self.offset = 0
            self.offsetMin = offsetMin
            self.offsetMax = offsetMax

        def update(self, st):
            st = st % (self.offTime + self.onTime)
            if st > self.offTime and self.offset == 0:
                self.offset = random.randint(self.offsetMin, self.offsetMax)
            elif st <= self.offTime and self.offset != 0:
                self.offset = 0

                #This class defines a renpy displayable made up of `number` of screen tear
    #sections, that bounce back and forth, based on ontimeMult & offtimeMult
    #and each piece is randomly offset by an amount between offsetMin & offsetMax
    class Tear(renpy.Displayable):
        def __init__(self, number, offtimeMult, ontimeMult, offsetMin, offsetMax, srf=None):
            super(Tear, self).__init__()
            self.width, self.height = renpy.get_physical_size()
            #Force screen to 16:9 ratio
            if float(self.width) / float(self.height) > 16.0/9.0:
                self.width = self.height * 16 / 9
            else:
                self.height = self.width * 9 / 16
            self.number = number
            #Use a special image if specified, or tear current screen by default
            if not srf: self.srf = screenshot_srf()
            else: self.srf = srf

            #Rip the screen into `number` pieces
            self.pieces = []
            tearpoints = [0, self.height]
            for i in range(number):
                tearpoints.append(random.randint(10, self.height - 10))
            tearpoints.sort()
            for i in range(number+1):
                self.pieces.append(TearPiece(tearpoints[i], tearpoints[i+1], offtimeMult, ontimeMult, offsetMin, offsetMax))

        #Render the displayable
        def render(self, width, height, st, at):
            render = renpy.Render(self.width, self.height)
            render.blit(self.srf, (0,0))
            #Render each piece
            for piece in self.pieces:
                piece.update(st)
                subsrf = self.srf.subsurface((0, max(0, piece.startY - 1), self.width, max(0, piece.endY - piece.startY)))#.pygame_surface()
                render.blit(subsrf, (piece.offset, piece.startY))
            renpy.redraw(self, 0)
            return render

#Define the screen for Renpy, by default, tear the screen into 10 pieces
screen tear(number=10, offtimeMult=1, ontimeMult=1, offsetMin=0, offsetMax=50, srf=None):
    zorder 150 #Screen tear appears above pretty much everything
    add Tear(number, offtimeMult, ontimeMult, offsetMin, offsetMax, srf) size (1920, 1080)
    on "show" action Function(hide_windows_enabled, enabled=False) #This makes sure UI is hidden
    on "hide" action Function(hide_windows_enabled, enabled=True)





####### A white noise effect, from the old RenPy documentation

image noise:
    truecenter
    "images/bg/noise1.jpg"
    pause 0.1
    "images/bg/noise2.jpg"
    pause 0.1
    "images/bg/noise3.jpg"
    pause 0.1
    "images/bg/noise4.jpg"
    pause 0.1
    xzoom -1
    "images/bg/noise1.jpg"
    pause 0.1
    "images/bg/noise2.jpg"
    pause 0.1
    "images/bg/noise3.jpg"
    pause 0.1
    "images/bg/noise4.jpg"
    pause 0.1
    yzoom -1
    "images/bg/noise1.jpg"
    pause 0.1
    "images/bg/noise2.jpg"
    pause 0.1
    "images/bg/noise3.jpg"
    pause 0.1
    "images/bg/noise4.jpg"
    pause 0.1
    xzoom 1
    "images/bg/noise1.jpg"
    pause 0.1
    "images/bg/noise2.jpg"
    pause 0.1
    "images/bg/noise3.jpg"
    pause 0.1
    "images/bg/noise4.jpg"
    pause 0.1
    yzoom 1
    repeat

#Have the noise faid in to 40%
transform noisefade(t=0):
    alpha 0.0
    t
    linear 5.0 alpha 0.40
