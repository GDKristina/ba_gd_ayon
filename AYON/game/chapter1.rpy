﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define t = Character("Thea")
define v = Character(None, what_outlines=[(5, "#000000")])
define steph = Character("Steph")
define psy = Character("Therapist")
define f1 = Character("Friend 1")
define f2 = Character("Friend 2")

# Declare images here. Start with Backgrounds and make seperate colums for sprites.

image bg fog = "images/bg/the_fog.png"
image bg apartment day = "images/bg/apartment_day.png"
image bg apartment night = "images/bg/apartment_night.png"
image bg balcony = "images/bg/the_balcony.png"
image bg city skyline green = "images/bg/city_skyline_green.png"
image bg city skyline red = "images/bg/city_skyline_red.png"
image bg city skyline grey = "images/bg/city_skyline_grey.png"
image bg laptop 20 = "images/bg/voice_call_full.png"
image bg laptop empty = "images/bg/laptop_empty.png"
image bg therapist green = "images/bg/therapist_green.png"
image bg mensa = "images/bg/mensa.png"

image bg glitch1 = "images/bg/glitch1.jpg"
image bg glitch2 = "images/bg/glitch2.jpg"

image black = "images/bg/black.png"
image red = "images/bg/red.png"


#### Declare sprites here.

image steph angry day = "images/sprites/steph/steph_angry_day.png"
image steph angry dark = "images/sprites/steph/steph_angry_dark.png"
image steph happy day = "images/sprites/steph/steph_happy_day.png"
image steph happy dark = "images/sprites/steph/steph_happy_dark.png"
image steph neutral day = "images/sprites/steph/steph_neutral_day.png"
image steph neutral dark = "images/sprites/steph/steph_neutral_dark.png"
image steph worried day = "images/sprites/steph/steph_worried_day.png"
image steph worried dark = "images/sprites/steph/steph_worried_dark.png"
image steph sad day = "images/sprites/steph/steph_sad_day.png"
image steph sad dark = "images/sprites/steph/steph_sad_dark.png"
image steph sigh day = "images/sprites/steph/steph_sigh_day.png"
image steph sigh dark = "images/sprites/steph/steph_sigh_dark.png"

image thea blush day = "images/sprites/thea/thea_blush_day.png"
image thea blush dark = "images/sprites/thea/thea_blush_dark.png"
image thea neutral day = "images/sprites/thea/thea_neutral_day.png"
image thea neutral dark = "images/sprites/thea/thea_neutral_dark.png"
image thea uncomfy day = "images/sprites/thea/thea_uncomfy_day.png"
image thea uncomfy dark = "images/sprites/thea/thea_uncomfy_dark.png"


# Declare sounds here.

define audio.siren_warning = "audio/siren_warning.mp3"
define audio.knock = "audio/knock.mp3"
define audio.school_bell = "audio/school_bell.mp3"
define audio.crowd_talking = "audio/crowd_talking.mp3"
define audio.night = "audio/ambience_night.mp3"
define audio.drone = "audio/drone.mp3"
define audio.darker_you = "audio/darker_you.mp3"
define audio.white_noise = "audio/white_noise_edit.mp3"

# The game starts here.

label start:

    stop music fadeout 4.0

    # Show a background. This uses a placeholder by default, but you can
    # add a file (named either "bg room.png" or "bg room.jpg") to the
    # images directory to show it.
label disclaimer:

    "Let me indulge you in a story during these trying times."
    "This is a story of mental health and current events."
    "This game is based on real events during the year 2020 and might feature resemblances to real people."
    "However, any of these resemblances are purely for artistic purposes."
    "Content Warning:"
    "There will be descriptions and/or depictions of self-harm, death/suicide, blood/gore, surgical procedures and mental illness."
    "Some parts of this game will also feature briefly flashing colors."
    "Please stay safe and take care."

label ch1:

label intro:
    play music "audio/drone.mp3" fadein 2.0
    scene bg city skyline grey
    with fade

    "The Fog came by surprise to most."
    "Yet others seem to have it seen creeping closer on the horizon for months."

    scene bg glitch1
    pause (0.2)
    show screen tear(20, 0.1, 0.1, 0, 40)
    pause (1)
    hide screen tear
    scene bg glitch2
    pause (0.2)
    scene bg city skyline red
    pause (0.75)

    "Either way, it was still too late."
    "Nobody expects their new year to start off like that, nobody is prepared for this during the holidays."
    "They tried evacuating everybody, but we were too many to fit into the bus."
    "They told us to wait for their return, that they would get the rest of us and left us stuck in the apartment complex."
    "The Fog worsened during the night, when the light pollution became visible."
    "We didn't know what to do."
    "What {i}would{/i} you even do in a situation like this?"

    stop music fadeout 3.0

    scene bg apartment day
    with fade
    pause (0.5)

    "A couple of weeks later you get woken up by the sound of a siren going off in the distance."
    "You starte and wait a couple moments for the heart palpitations to fade."
    pause (1)

    #play the siren here
    play sound "audio/siren_warning.mp3"



    "{font=YesYesNo.ttf}Attention to all citizens.{/font}"
    "{font=YesYesNo.ttf}Please keep your windows closed.{/font}"
    "{font=YesYesNo.ttf}The current contamination amount has risen by 3.6 milligramms per cubic meter.{/font}"
    "{font=YesYesNo.ttf}Try to stay inside or, if you have to go outside, remember to not spend more than 45 minutes in open air and wear your provided safety equipment that covers your mouth and nose.{/font}"
    "{font=YesYesNo.ttf}Once you return, make sure to wash all your clothes of all contaminants with soap or chemical cleaners like bleach.{/font}"

    stop sound

    "Your heart drops at the realization."
    "There won't be another bus."

    pause (0.5)

    "Once you collect yourself, you sink back into your pillows and start to think."
    "Which day is today?"
    "How many days has it been?"

    menu:
        "Today is Wednesday":
            "You had class on Monday and went grocery shopping on Tuesday."
            "That would make today ... Wednesday, right?"
            jump monday

        "Today is Thursday":
            "You had class on Monday, went grocery shopping on Tuesday and did nothing on Wednesday."
            "That would make today ... Thursday, right?"
            jump monday

        "Today is Friday":
            "You had class on Monday, went grocery shopping on Tuesday, did nothing on Wednesday."
            "Wait..."
            "What did you do on Thursday?"
            pause (0.25)
            "You can't remember, but it definitely feels like it's Friday."
            jump monday

label monday:
    "No, no, no."
    "No, that's not right."
    "Yesterday was Thursday."
    "No, yesterday was ... Monday?"
    "Yesterday was..."
    pause (1)

    "Yesterday was..."
    "April?"

    scene bg apartment day
    "Your eyes snap open again and hasitly try to find your phone on the nightstand."
    "It says 09:07. Monday. August 10th."
    "You rub your eyes."
    "That doesn't seem right."
    "You look again."
    "It still says August 10th."
    "You are sure that yesterday was April 30th."
    pause (0.25)
    "You jump out of bed."
    "You have an appointment in 30 minutes."



label voice_call:

    scene bg apartment day
    with fade

    "It has been a calm week."
    "Well, relatively."
    "The usual."
    pause (1)

    scene bg laptop 20
    with fade

    "You managed to wake up 10 minutes before the start of your lecture, managed to make coffee but not breakfast."
    "You could always eat during the lecture."
    "No time for brushing your teeth, you're still in your pajamas, hair unkempt, eyes barely open."
    "You yawn into the microphone of your headset."
    "Your camera isn't on."
    "Nobody's is."
    "You stare into the 20 different icons in front of you."
    "You never heard half of these names."

    "You blink."

    scene bg laptop empty

    "You're alone in the call."

    menu leave_call:
        "Leave the Voice Call":
            "You can't find the leave button."

    menu leave_call_2:
        "Try again":
            "You find it, eventually."
            "You leave the call."
            "And yet-"
            "And yet you still see the call open, your icon inside of it."

    menu call_dilemma:
        "Leave it be":
            "You close the apllication."
            "You know you left, it's probably just a bug."
            "Your webcam is turned off, nobody would be able to spy on you."
            "Once you log out, it surely will disappear."
            jump voice_call_cont

        "Try to fix it":
            "Maybe you did something wrong?"
            "You enter the voice call again."
            pause (1)

            "Your icon appears twice now."
            "You're in the call with yourself."
            "You try to exit the voice call again."
            pause (1)

            "The application crashes."
            "Well..."
            "That's one way to solve the problem."

label voice_call_cont:

    scene bg apartment night
    with fade
    "You get up and stretch."
    "It's 7 pm."


label scavengers:

    play music "audio/ambience_night.mp3" fadein 2.5

    scene bg balcony
    with fade

    """
    You decide to take a breather on the balcony.

    Cool air hits your face and you inhale deeply.

    Sometimes it's hard to notice how stuffy the air gets in your apartment when you don't leave.
    """
    pause (1)

    "You stare ahead to the building on the opposite street, when suddenly something startles you."
    play sound "audio/knock.mp3"
    pause (0.5)

    "You turn around and see Stephanie, your neighbour, standing at the door between your balconies."

    steph "Hey! Can you let me in?"

    "You open the door."

    show steph happy dark at left
    show thea neutral dark at right
    steph "Hi! Sorry to barge in like that, haha."
    hide steph happy dark
    show steph neutral dark at left
    steph "The filter on my balcony broke a few weeks ago and I couldn't use it."
    steph "I saw that your balcony lights were on and wanted to check in."

    "She seems to wait for some kind of response, so you just nod."
    "She stares at you for a bit until she sighs and rummages in her pockets."

    hide steph neutral dark
    show steph worried dark at left
    steph "So, Thea, how are you?"
    steph "I haven't seen or heard from you in ages."

    "She's pulling out a box of cigarettes."
    "You hate it when she smokes near you."

    t "I'm fine. Just busy."

    "Steph lights her cigarette and looks like she's trying to figure out if you're lying."

    menu balcony:
        "Lie":
            hide steph worried dark
            show steph neutral dark at left
            steph "You're still studying, right?"
            "She says and blows out some smoke."
            hide steph neutral dark
            show steph worried dark at left
            steph "Are you really okay?"

            hide thea neutral dark
            show thea uncomfy dark at right
            t "No, I'm fine, really."
            t "Just the usual."
            hide thea uncomfy dark
            show thea neutral dark at right
            t "I think we're all kind of struggling a little right now."

            "Steph give you another side glance and takes a pull off her cigarette again."
            "Neither of you say anything for a while."
            jump scavengers_cont

        "Be honest":
            $ vtext = spooktext(15)
            hide thea neutral dark
            show thea uncomfy dark at right
            show noise:
                alpha (0.02)
            v "No."
            v "No, you can't be honest with her."
            v "Sure, she's nice and all."
            v "Sure, she gives good advice sometimes."
            show noise:
                alpha (0.035)
            v "But it's not like you {i}know{/i} each other."
            v "She's just trying to be {i}polite{/i}."
            v "She came to speak to {i}you{/i}, so something must have happened."
            show noise:
                alpha (0.05)
            v "Don't worry her any more. She's nice. She doesn't deserve it."
            v "You don't deserve[vtext]it"
            hide noise

            steph "Thea?"
            hide thea uncomdy dark
            show thea neutral dark at right
            t "H-huh?"
            steph "Is it that bad? You spaced out for a bit there."
            hide thea neutral dark
            show thea blush dark at right
            t "No, no. It's fine."
            hide thea blush dark
            show thea uncomfy dark at right
            t "I just remembered that I have a deadline coming up next week."
            hide thea uncomfy dark
            show thea neutral dark at right
            hide steph worried dark
            show steph neutral dark at left
            steph "Oh, right!"
            hide steph neutral dark
            show steph happy dark at left
            steph "You're still studying, right?"
            "She says and blows out some smoke."
            hide steph happy dark
            show steph worried dark at left
            steph "Are you really okay?"

            hide thea neutral dark
            show thea uncomfy dark at right
            t "No, I'm fine, really."
            t "Just the usual."
            hide thea uncomfy dark
            show thea neutral dark at right
            t "I think we're all kind of struggling a little right now."

            "Steph give you another side glance and takes a pull off her cigarette again."
            "Neither of you say anything for a while."
            jump scavengers_cont

label scavengers_cont:

    steph "So, uhm..."
    steph "It's been a while since the Scavengers left, huh?"
    t "A week, tomorrow."

    "When the city went into lockdown, the only way to get any kind of groceries or neccesseties was by going into the untouched stores."
    "But because you were quarantined in the apartment complex, the only way was to have someone deliver them to you."
    "And that stopped because the police blocked off all roads leading to your complex in particular."
    "Out of frustration a few from your building volunteered, said they would check the stores for anything under the condition that they'd be provided with safety equipment."

    steph "Can I be honest with you?"
    t "..."
    steph "I don't think they'll come back."
    t "Why?"
    steph "It's been a {i}week{/i}, Thea."
    hide steph worried dark
    show steph sad dark at left
    steph "Someone from my class is in that group."
    steph "I've tried messaging him but nothing's been getting through."
    t "His phone is probably just dead at this point."
    hide steph sad dark
    show steph worried dark at left
    steph "No, I mean over the intercoms."
    steph "I asked him for the frequency before they left just in case and nothing has been getting through since Tuesday."
    steph "The batteries should last for up to a month, so something might be blocking the signal."
    t "Even if..."
    t "There's nothing we could do, is there?"
    hide steph worried dark
    show steph sad dark at left
    steph "Probably..."
    steph "I ... I just don't know."
    steph "I can't help but worry. I hope they make it back soon."
    "She finishes her cigarette."

    hide steph sad dark
    show steph happy dark at left
    steph "Sorry to bother you with this, I just..."
    t "Felt like you needed to talk?"
    hide steph happy dark
    show steph neutral dark at left
    steph "Yeah."
    steph "Yeah, that would be it."
    t "No, it's okay."
    ### add an exhusted expression to steph
    hide steph neutral dark
    show steph sigh dark at left
    steph "We've been stuck in here for {i}months{/i}."
    steph "I haven't really talked to anybody since my classes ended last week."

    hide steph sigh dark
    show steph neutral dark at left
    steph "I never see you downstairs. Don't you feel lonely sometimes?"
    v "Do you?"
    hide thea neutral dark
    show thea uncomfy dark at right
    t "I-"
    hide steph neutral dark
    show steph sad dark at left
    steph "No, that was rude of me to ask."
    steph "Of course we all feel weird in the current situation. I didn't mean to make this uncomfortable for you."
    t "No, Steph, listen-"
    hide steph sad dark
    show steph sigh dark at left
    steph "No, you're going to say \"No, it's fine\"."
    steph "You're always {i}this is fine{/i} this, {i}this is fine{/i} that."
    t "N-no, Steph, I ... I really don't mind, okay?"
    t "It's {i}fine{/i}."
    "Steph sighs a last time, before putting the box of cigarettes away."
    steph "Okay then."

    hide steph sigh dark
    show steph neutral dark at left
    steph "I think I'm going back inside, if you don't mind."
    steph "It was really nice talking to you, Thea."
    t "Yeah, uh, same here."
    hide steph neutral dark
    show steph happy dark at left
    steph "If you want to talk, feel free to knock anytime, yeah?"
    steph "I don't have classes anymore and we're all stuck in here, so..."
    steph "You know, maybe you'll feel a little lonely and need someone to vent."
    hide steph happy dark
    hide thea uncomfy dark
    "And with that she says good night to you and leaves."
    v "When you feel lonely, huh?"
    "Do you?"

    stop music

label lonely:

    #music.set_volume(0.5, delay=0, channel=u'music')
    play music "audio/darker_you.mp3"

    scene bg therapist green

    psy "Let's do a little mental exercise today."
    t "Okay."
    psy "Imagine you're on an island."
    psy "The island is empty, no animals, no other people. You have everything you'd need to survive."
    t "M-hm."
    psy "You can take anything or anyone you want with you, but you can't leave the island."
    psy "What would you take with you?"
    t "..."
    t "Some books. If I had internet and access to electricity my computer as well. Clothes."
    pause (1)
    psy "Okay."
    psy "And if you could take someone with you, who would it be?"
    t "I don't know. Nobody."
    pause (1)

    "She writes something down."
    show noise:
        alpha (0.05)
    v "This was a test."
    v "You failed it."

    psy "Okay."
    psy "Do you feel lonely sometimes?"
    psy "Do you feel alone, even though you're with other people?"
    t "I don't know."
    t "No."
    pause (2)
    psy "Do you have friends, Thea?"
    t "Of course."
    hide noise

    stop music

label friends:

    play sound "audio/school_bell.mp3"

    scene bg mensa
    with fade

    play music "audio/crowd_talking.mp3"

    show noise:
        alpha (0.1)

    t "I'm back."
    f1 "Hey Thea, while you were gone, we came up with a game for you!"
    t "For me?"
    f2 "Yeah!"
    t "Cool! What kind of game is it?"
    f1 "The rules are really simple!"
    "Your friend pulls out something from her pocket."
    f1 "I got a stopwatch and we stop the time you can go without talking!"
    pause (1)
    "Huh?"
    t "No talking? At all? And you?"
    f2 "No, no talking. And it's just you."
    t "Hm, I don't know..."
    t "What if I don't want to play?"
    f1 "The you can't play with us during break tomorrow!"
    t "Huh?! What?!"
    t "N-no, okay, I'll play the game!"
    f1 "Okay, ready?"
    "You nod."
    f1 "Go!"

    "Instinctually, you hold your breath, until a few seconds pass and you notice that this strategy isn't working."
    "You exhale and close your mouth quickly again."
    pause (1)
    "A minute passes."
    pause (2)
    "Another minute passes."
    pause (3)
    "Five minutes pass."
    "All three of you have already finished lunch."
    pause (4)
    "Another minute passses."
    "You remember the movie your friends wanted to see."
    "Your mum said that she'd drive you to the cinema on the weekend."

    t "Oh! About that movie-!"
    f2 "{i}Stop!{/i}"
    f1 "Oh wow! That's a new record! 8 minutes!"
    t "Is it...?"
    f1 "Yeah! That was fun while it lasted!"
    t "Well you seemed to have fun, so that's great!"
    t "A-about that movie..."
    t "My mum said she'd drive us..."
    f2 "Really?!"
    f2 "That's so cool Thea, thank you!"
    "Your friends start talking about how excited they are to see the movie."
    "You're happy that they're having fun."

    stop music

    play music "audio/white_noise_edit.mp3"

    show noise:
        alpha (0.15)

    "You often think back to days like these."
    "How much fun they must've had."
    "Yet sometimes you also think about why it was so hard to keep people in your life after high school."

    show black:
        alpha (0.05)

    v "It must be you."
    v "You're the only common factor."
    "People have always seemed nice and interested when they met you for the first time."
    "It never lasted long."
    "Maybe they saw that rotting thing inside of you[vtext]."
    "They saw that there was nothing worthwhile."
    "They saw that your personality didn't make up for the lack of looks."
    "Some of them stayed."
    v "Probably because they were too nice to leave."
    "Or because they didn't want to make themselves look bad by leaving."

    show black:
        alpha (0.1)

    "People like the {i}idea{/i} of you."
    "They like the way you nod along their conversations, add some \"ah\"s and \"oh\"s and \"mhm\"s."
    "They like that they can always come to you when they are fighting with their friends, because you will never turn them away."
    "They like the way that you'll do everything to make them like you."

    show noise:
        alpha (0.2)

    show black:
        alpha (0.3)

    "You can't change the truth."
    v "You can't hide the fact that you're a parasite, leeching off of any social interactions you can get too fill that hole inside your chest."
    v "You abuse anybody's kindness. You want them to like you."
    v "They leave because all you think about is you."
    v "It's you[vtext]."

    "Have you never wondered why people who have to deal with you every day hate you?"
    "You hear the shift in their voice, subtle, but noticebale."
    "You see their little gestures, how they try to leave just because you are there?"
    "How people who don't know you that well, who see you once a week or less, are not like that?"
    "That they worry? Are nice?"
    v "Because they don't know."
    "Because they don't know you."

    stop music

    "How much longer will you deny the truth?"
    show screen tear(20, 0.1, 0.1, 0, 40)
    pause (1)
    jump words
