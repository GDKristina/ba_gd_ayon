﻿# The script of the game goes in this file.
$ vtext = spooktext(80)

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define l = Character("Lucas")



# Declare images here. Start with Backgrounds and make seperate colums for sprites.

image bg hallway = "images/bg/hallway.png"
image bg apartment day ch3 = "images/bg/apartment_day_3.png"
image bg laptop red = "images/bg/laptop_red.png"


#### Declare sprites here.
image lucas neutral day = "images/sprites/lucas/lucas_neutral_day.png"
image lucas happy day = "images/sprites/lucas/lucas_happy_day.png"
image lucas worry day = "images/sprites/lucas/lucas_worry_day.png"



# Declare sounds here.

define audio.siren_warning = "audio/siren_warning.mp3"


# The game starts here.

label family:

    # cut due to time contraints
    jump volunteer

label volunteer:

#    scene bg apartment day ch3
#    with fade

#    show thea neutral day
#    t "I'll go."
#    show lucas worry day at left
#    l "What?"
#    l "Thea, are you sure?"
    #t "Yes? Why shouldn't I be?"

#    "The way he looks at you. You don't like it."
#    "Like he's saying \"You can't do this\"."

#    show steph worried day at right
#    steph "Well, I mean, it's just a one day trip."
#    steph "It only takes one hour to get there by train."
#    l "Yeah, but into downtown?"
#    l "I doubt they'll let you in there."
#    t "Well, I do have to take the train in that direction anyway, I'll just exit a few stations earlier."
#    l "I still don't think this is a good idea."
#    l "What are you even trying to find there?"
#    t "Think about it."
#    t "Why would they put the scavengers under arrest for this long?"
#    t "They technically weren't trespassing, the officer confirmed that with the ones keeping under lockdown."
#    l "Yeah, so?"
#    t "They must've {i}seen{/i} something there."
#    steph "Sorry, I'm afraid I don't follow."
#    t "Okay, so what if there is {i}something{/i} in downtown, something we're not supposed to know about?"
#    t "Maybe they're hiding something there?"
#    l "Thea, who is they?"

    #"{i}Them.{/i}"
#    "You've seen them before."
#    "They did tests on you as a child."

#    t "Where do you think The Fog came from?"
##    l "Well, the sky?"
#    l "I don't study meteorology, so I don't know the specifics."
#    t "Yeah, but why did they put us and Newmans Yard into lockdown specifically?"
#    t "Because we're the adjacent districts. We're close."
#    steph "Don't you think this is a little far fetched?"
#    t "No, think about it!"

#    "You get up and point outside across your balcony to where the downtown would approximately would be."

#    t "Look at that building there, I think that's the news station."
#    t "Why is it more heavily obscured than the others?"
#    l "Thea, that's literally how the atmosphere works."
    #t "Ugh! I should've never told you two!"

#    "They exchange glances."
#    v "They think you're crazy."
#    v "They don't know what you saw."

#    steph "Listen, Thea."
    #steph "We can't stop you from going tomorrow."
#    steph "Just ... be careful, please."
#    "Lucas sighs deeply and runs his hands through his hair."
#    l "I still think this isn't a good idea."
#    l "I don't want to sound like an asshole, but I think you shouldn't go."
#    t "Why not?"
#    l "Thea, you passed out in the hallway just like that a week ago."
#    l "What if that happens while you're there?"
#    t "It won't!"
#    l "You can't know that!"
    #hide steph worried day
    #show steph angry day at right
    #steph "Calm down you two!"
    #hide steph angry day
    #show thea worry day at right
    #steph "She's going to be fine for an hour or two, Luke."
    #"He crosses his arms in front of his chest and sighs."
    #l "We're not your parents."
    #l "I know you'll just go anyway not matter what we say."
    #steph "Well, if it calms your nerves, I still have that transmitter from when the first group left."
    #t "But you had issues getting a signal through?"

label noise:
    play music "audio/darker_you.mp3"
    scene bg laptop red

    "How would it look if you left {i}right now{/i}?"
    v "Bad."
    "You would make everything worse."
    v "You're not in the position to make demands."
    v "Not after you posted a single meme in the group chat, setting off this whole disaster."
    "Not after you had to open up, in defense, about supposedly being clinically depressed because someone had the audacity to say that \"depression had it's benefits\"."
    "You should just apologize."
    "You understand that they're angry with you."
    "You're angry with yourself."
    "You did this to yourself."

    show noise:
        alpha (0.15)

    dan "Thea, I just don't think you do a good job with project management."
    v "You can't do anything right."
    t "Well... I didn't really want to do it in the first place..."
    dan "Then why did you insist on doing it?"
    v "You insert yourself into everything, Thea."
    t "Because you always said that you don't need anyone who writes the story."
    t "Because there's never time to do that."
    s "Thea, you yourself said that story isn't important in this project."
    v "This is your own fault."
    t "Yeah, because you said that you wanted more creative freedom..."
    dan "Because you always want to have it your way!"
    v "You can't have anything!"
    v "You're making this harder for everybody."
    t "This was the first time! Is it my fault that I gave you what you wanted?"
    s "No, Thea, that's not it, don't twist our words!"
    v "You're a liar."
    v "You do nothing but lie."
    v "You contribute nothing."

    show noise:
        alpha (0.2)

    dan " You should've done more management, that is all."
    v "You've done nothing in this project, once again."
    v "You're useless."
    t "First you say that I do too much and now you say I do too little?"
    "You choke on your tears."
    "This is unfair."

    show noise:
        alpha (0.3)

    show black:
        alpha (0.1)

    t "It doesn't matter what I do, I'm always the one at fault!"
    t "No matter what I do, you always say that I did something wrong!"
    "Every single project you worked on failed."
    "You're the only common factor."
    dan "Hey, {i}no management{/i} is not {i}less management{/i}!"
    v "You can't do anything right."
    v "Stupid."
    v "You're stupid."
    v "{nw}[vtext]"
    s " Honestly, maybe this just isn't for you, Thea?"
    v "You wasted 30.000 euros and 3 years of your life to do this."
    v "You leech."
    v "You waste of space."
    c "Thea, stop getting so worked up, please."
    c "This isn't about you as a person, this is about the way you work."
    v "You are your work."
    v "There is no way this isn't personal."
    v "You will never find work."
    v "Nobody wants to work with you."
    v "This is the reason you couldn't find an internship."
    t "If this isn't about me as a person, then what is it?"
    "Your voice cracks."
    "You're probably completely incoherent through your headset."

    s "All we're saying is that maybe this isn't the industry for you?"
    "This was your Plan B."
    "And Plan C."
    "And Plan D."
    v "You can't work a normal job. You're sick, physically and mentally."
    v "You're absolutely useless."
    v "A waste of skin."
    v "Just die."

    show black:
        alpha (0.2)
    "Die."

    show black:
        alpha (0.25)

    "Die."

    show black:
        alpha (0.3)

    "Die."

    show black:
        alpha (0.35)

    "Die."

    show black:
        alpha (0.4)

    "Die."

    show black:
        alpha (0.45)

    stop music

    "D I E"

    scene black
    "Just perish."

label knife:
    # cut due to time contraints
    jump april



    return
