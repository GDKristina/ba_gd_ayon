﻿# The script of the game goes in this file.
$ vtext = spooktext(15)

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define l = Character("Lucas")



# Declare images here. Start with Backgrounds and make seperate colums for sprites.

image bg beach = "images/bg/beach.png"



#### Declare sprites here.
image lucas neutral day = "images/sprites/lucas/lucas_neutral_day.png"




# Declare sounds here.

define audio.siren_warning = "audio/siren_warning.mp3"


# The game starts here.

label april:

    scene black

    """
    There are harsh white lights above you.

    You can't make anything out.

    You're laying down on something hard, yet you move.

    You hear muffled voices.

    They sound agitated.

    You wish they would stop being so noisy.

    You want do close your eyes, but you're frozen.

    Images flash in your head.

    You're with someone.

    A group.

    You're in the elevator going up to the 8th floor.

    Nervousness is thick in the air.

    You're all in hazmat suits.


    The doors open.

    There's a sharp pain in your chest.

    You cough and something spills from your lips, stains the inside of your helmet.

    It's pitch black.
    """

    "\"Hey!\", someone shouts."
    "It sounds so far away."
    "A thud."
    "\"Man down! I need some help here!\""
    "More liquid spills from your lips."
    "It fills your helmet."
    "You breathe it back in and cough more."
    "\"Call an ambulance!\""
    "Somebody takes your helmet off."
    "\"Fucking hell!\""
    "\"God, what is this?!\""
    "You can't move."

    show screen tear(20, 0.1, 0.1, 0, 40)

label the_fog:
    play music "audio/darker_you.mp3"

    scene bg fog
    hide screen tear

    """
    You cough.

    The Fog is so thick you can barely see ahead of you.

    You keep going.

    You hear the signal of your transmitter.
    """

    steph "Thea?"
    steph "Thea, can you hear me?"

    """
    You don't answer.

    You're not going back.

    There's no coming back from this.

    Your eyes tear up. It's getting harder and harder to see.

    You're almost there.

    Almost.
    """

    stop music

label death:

    scene black

    """
    Have you died before?

    Have {i}you{/i} died before?

    There is a memory somewhere, hidden deep inside you.

    You're just a baby. You're so small.

    You were premature. Two months early.

    They extracted you from the womb like a parasite.

    Just moments after, your heart stopped.

    They had to revive you moments after you were born.

    You had expired.

    Yet you're still here.

    You're undead.

    You're a crime against nature.

    You're not supposed to be alive.

    You want to end your life because it's not meant to be.

    There must be a primal instinct that knows that there's something fundamentally wrong with you.

    Some kind of automated kill switch.

    {i}End runtime if x{/i}

    Yeah, that must be it.

    Why else would you have spent five years convinced that you're not human?

    Why you had to check every day that your blood still ran red inside your veins?

    That you had a heartbeat? Sometimes it got hard to feel it, even the doctors were surprised sometimes.

    You're not human, you're a creature.

    An abomination.

    It wasn't meant to be, this life of yours.

    End it.

    Once and for all.
    """

label calm:
    play music "audio/beach.mp3" fadein 3.0

    scene bg beach

    """
    You're at the beach again.

    You feel so cold, but the sand is warm.

    You rake your hands through it and think about nothing.

    Why wasn't it always like this?

    Why did you have to wake up every time?

    You want to spend the rest of eternity here.

    There is nobody to judge you.

    No obligations.

    No uncertainties.

    Stillness.

    Peace.

    Quiet.

    You don't even have a physical form if you don't want to.

    You're ethereal.

    You can't die, yet you do not live either.

    The sea beckons you.

    It wants to drown you again.

    Make you wake up.

    But you don't want to.

    Time here is meaningless.

    Just a little longer, you tell yourself.
    """

    stop music fadeout 3.0

label conclusion:

    scene bg apartment day ch3
    with fade

    """
    You open your eyes.

    You're in a bed.

    It's not yours.

    You feel cold.

    Your throat hurts.
    """
    "\"She's awake! God, she's awake!\""

    t "Please stop shouting."
    """
    You say, yet no sound leaves your mouth.

    You turn your head. You see Steph sitting on a chair across you.

    She looks like she has been crying.

    Why?

    Oh.

    Right.
    """

    show steph worried day at right
    steph "Oh God Thea, why do you do things like this?"
    "She looks like she's about to cry again."
    steph "We were worried sick about you!"
    t "Wha... Where....?"
    steph "You're at the hospital."
    steph "You can't just leave like that!"

    "She starts crying again."
    v "You did this."
    v "You made her cry."
    "You're tired."

    show lucas worry day at left
    l "Thea!"
    "His nose is a little red. He grabs another chair after he puts a mug down and sits down next to Steph"
    "You turn your head a little more."
    "Why are there so many people in your room?"

    steph "The scavengers went looking for you after we noticed that you were gone."
    steph "They found you near the central station. You were barely concious."
    steph " We called an ambulance and they said that you'd wake up in a few hours."
    "You open your mouth to ask something."
    "You just cough."
    "Lucas gets up again and pours you a glass of water."
    "Your hands shake when you reach for it."

    t "What about the black stuff?"
    steph "The black stuff?"
    t "I got sick, in the elevator."
    t "The Scavengers, they called an ambulance because I started coughing again."
    pause (2)
    steph "Thea, you were having a nightmare."
    steph "You were alone."
    steph "There was no black stuff."

    "You cough again."
    "God, it hurts."

    t "Ow..."
    l "The doctor said you probably just collapsed from exposure to The Fog."
    t "Am I going to die?"

    "Lucas and Steph exchange glances again."
    steph "Thea, you just inhaled some fumes that aren't good for you."
    steph "You're not going to die."

    "You close your eyes."
    "A shame."
    l "Why did you leave? We tried contacting you."
    t "I wanted to leave."
    "Your voice sounds hoarse."
    t "I am sick of this."
    steph "Well, according to the news, the lockdowns will be lifted in a couple weeks once the contamination levels sink below the threshold."
    t "No, I mean, I'm sick of {i}this{/i}."

    """
    You gesture vaguely to yourself.

    That was a mistake.

    You immediately see their expressions changing.

    You know that expression.

    You hate that expression.

    It's the expression of things falling into place, of gears rotating in someone's head, of things finally making sense.
    """

    l "Thea..."
    t "No."
    t "Don't."

    "There's a heavy silence between you for a while."
    steph "You could've told us, you know?"
    "Her expression is kind but you don't like that she sounds so sad."
    "She shouldn't be sad because of you."
    v "You made your friends sad."
    v "Again."
    t "No, I couldn't."
    "Lucas takes the empty glass from your hand and Steph immediately takes it into hers carefully."
    "You only now notice the IV and follow it with your eyes."
    "Painkillers. Thank God."
    steph "We love you, you know that, right?"
    v "She's sayig this to comfort you."
    t "I know."
    steph "Then why?"
    steph "Why do this?"
    t "I'm tired."
    "Lucas places his hand on Steph's shoulder."
    l "Don't bombard her like that, she just woke up."
    l "Do you want us to give you some time?"

    "It's fine."
    t "Please."
    "Steph squeezes your hand one more time before she gets up."
    steph "Take the time you need, okay?"
    play music "audio/requiem.mp3"

label outro:

    """
    In the end, you were grateful, somehow.

    You weren't ready to go.

    You felt guilty, because you hadn't told them all the things you wanted to.

    Because you haven't told them how much they meant to you.

    Their little gifts, their smiles, their stories.

    You wanted to tell them that you loved them, that they are wonderful people, that you didn't deserve them, that they're breathtaking.

    You wanted to thank them, for all the little things they did that kept you going.

    You wanted to do one last selfless act before you go.

    You wanted to give them a kind of parting gift.

    You wanted them to both forget and remember you.


    Maybe you were given a second chance.

    And a third.

    And a fourth.

    Again and again.

    Until you finally did everything you had to.

    And then, and only then, you were allowed to go.

    Somehow this gives you the peace of mind to sleep every night.

    You know that there will be no way to fix all of this.

    But you also know that it didn't matter, as long as your loved ones were safe and sound.

    That was the least you could do.

    For them.

    As a final thanks.
    """

    pause (2)

    "Thank you."

    stop music fadeout 2.0
    pause (1)



return
