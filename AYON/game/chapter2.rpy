﻿# The script of the game goes in this file.
$ vtext = spooktext(15)

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define t = Character("Thea")
define c = Character("Charlotte")
define s = Character("Stacy")
define dan = Character("Daniel")
define a = Character("Alex")


# Declare images here. Start with Backgrounds and make seperate colums for sprites.

image bg laptop 4 = "images/bg/laptop_4.png"
image bg beach = "images/bg/beach.png"
image bg apartment day ch2 = "images/bg/apartment_day_2.png"
image bg mailbox = "images/bg/mailbox.png"
image bg hallway = "images/bg/hallway.png"


#### Declare sprites here.
image lucas neutral = "images/sprites/lucas/lucas_neutral_day.png"
image lucas happy = "images/sprites/lucas/lucas_happy_day.png"
image lucas worry = "images/sprites/lucas/lucas_worry_day.png"



# Declare sounds here.

define audio.siren_warning = "audio/siren_warning.mp3"


# The game starts here.

label words:
    hide screen tear
    hide noise
    hide red

    scene bg laptop 4

    "Thea? Are you still there?"
    t "Ah, sorry! Spaced out for a sec."
    t "Yeah, uh, could you repeat that?"
    s "We were talking about what lectures we are taking next semester, everything being online still."
    t "Oh, I actually haven't thought about that."
    t "I'm missing some credits, so I guess I'll just take both from our module?"
    c "Isn't that a bit much?"
    t "Well it's either that or some photography course."
    c "Just make sure it doesn't get too much, okay?"
    t "Well, there's nothing I can do, is there?"
    t "I'm not doing any development either way, I think I can handle managing two teams."
    s "Oh, by the way, have you heard about Newmans Yard?"
    c "Isn't that where you live, Thea?"
    t "It's nearby, why?"
    s "Apparently they shut the complete district off, put it into quarantine."
    s "There's police and barricades and stuff everywhere!"

    "You grab your phone and text Steph."
    t "{font=YesYesNo.ttf}Hey Steph, do you know if the scavengers went into newmans yard?{/font}"
    steph "{font=YesYesNo.ttf}Yeah, why?{/font}"
    t "{font=YesYesNo.ttf}Someone told me that they put it into quarantine too{/font}"
    steph "{font=YesYesNo.ttf}What? Really?!{/font}"
    steph "{font=YesYesNo.ttf}Did they tell you if the scavengers made it out?{/font}"
    t "{font=YesYesNo.ttf}No{/font}"
    steph "{font=YesYesNo.ttf}Shit...{/font}"
    steph "{font=YesYesNo.ttf}Hold on...{/font}"

    t "Do you know anything else? Did someone make it out before they quarantined it?"
    s "No, why?"
    t "My neighbour knows someone who went looking for salvageables there and they haven't come back."
    c "Oh, fuck..."
    pause (1)
    c "Wait."
    c "Did you just say {i}salvageables{/i}?"
    t "Yeah?"
    t "What would you call them?"
    "You start to get a little annoyed."

    t "You know what I mean. It doesn't matter."
    dan "Well, it wouldn't if {i}you{/i} weren't the one saying it, Ms. Dictionary Definition."
    show red with fade:
        alpha (0.1)
    show noise:
        alpha (0.05)
    t "Excuse me?!"
    t "There is a difference between me making up a new, more convienent word that everybody understands, and incrorrectly using a word that already exists with a definition."
    s "Oh god, shut up you two."
    dan "Sorry, I'm not the one being unnecessarily pedantic on certain things and only doing so when it's not about me."
    show red:
        alpha (0.2)
    t "That's not true! I'm literally just-"
    dan "You're {i}literally{/i} what?"
    dan "Just admit that you're wrong for once and I'll drop it."
    show red:
        alpha (0.3)
    t "I'm not wrong!"
    c "Come on, leave Thea alone Daniel, she's-"

    hide red
    hide noise

    show black at center
    "You leave the voice call."

label beach:

    scene bg beach
    with fade

    play music "audio/beach.mp3" fadein 3.0


    """
    There is black sand everywhere.

    It feels warn underneath your feet.

    You've been here before.

    Many, many times.

    A place where the sand is black, the sea is dark and the sky is an eternal void dotted with stars and adorned with two crecent moons.

    A place that scared you at first, but gives comfort now.

    A place that is not real.

    You lay down in the sand and close your eyes.

    You think of yourself as someone who is good with words. Sometimes.

    And yet you find yourself unable to describe things to people in a way they would understand.

    The feeling of being awake at 2 am, standing on the balcony when everything is asleep and it feels like it's just you on this earth.

    The thrill, the gust of air before the train arrives and you're standing just a little bit too close on the edge of the platform.

    That feeling of waking up, the moment between sleep and reality.

    The thrumming in your chest when you know that your loved ones are safe, have luck in their live and careers, when you're stuck in a downward spiral.

    The stabbing in your eyes in the middle of the night when you cry as quiety as possible, to not wake up your parents.

    The shiver, the sudden drop as if you're on a rollercoaster out of nowhere. Hoe everything feels brighter, clearer, after that.

    You get up and step into the waves.

    You recognize the feeling of hands grabbing your ankles, pulling you downwards.

    The feeling of asphyxiation when the water fills your lungs.

    You wake up.
    """

    stop music fadeout 2.0

label tea:

    scene bg apartment day ch2
    with fade

    "You sigh."
    "You've been staring at your screen for hours and yet it still doesn't feel like you're making any progress."
    "Your team is going to kill you if you don't finish the schedule on time."
    pause (0.3)
    "Well, they're going to kill you {i}anyway{/i} because they won't like it as per usual"
    "but having done {i}something{/i} is better than having done {i}nothing{/i}."
    pause (0.5)
    "You could really use a break right now."
    "You're going to make yourself some tea."

    "What kind of tea do you want?"

    $ tea_empty = False

    menu tea_choice:
        "Herbal Tea" if tea_empty == False:
            "Some herbal tea would do well for your nerves."
            "You open the cupboard in your kitchen aisle and reach for the box of chamomile."
            "Sweet, sweet chamomile."
            pause (0.5)
            "It's empty."
            "You cringe and remember."
            "Right."
            "You wrote that on the shopping list you gave to the Scavengers."
            "Damn it."

            $ tea_empty = True
            jump tea_choice

        "Green Tea":
            "Green tea is always a solid choice."
            "You feel a little decadent and reach into the cupboard in your kitchen aisle for the Jasmine Tea."
            "You like the way it smells, kind of flowery and sweet, but your mum said it reeks of cheap perfume."
            "Well, it tastes good anyway."
            jump tea_cont

        "Black Tea":
            "Some good of caffeine would do you some good right now."
            "Did you know that there's more caffeine in black and green tea per 100 gramms compared to the same amount of ground coffee?"
            "You didn't know that until yesterday."
            "You reach into the cupboard of your kitchen aisle and take out the box of Earl Grey."
            jump tea_cont

label tea_cont:

    "You measure the right amount of tea leaves and add them to a tea bag while the water's heating up to the right temperature."
    "The box says \"steep for 2 minutes\"."
    "No biggie."
    "You sit back down at your desk and check the time."
    "12:28."
    "So your tea's is going to be ready at 12:30. Perfect!"
    "You stare at the clock on the bottom of the screen, waiting for time to pass."
    pause (2)

    "This is getting boring."
    "You're going to add one point to the schdule in the meantime and then take out the tea bag."
    "You finish and check the time."
    "It's 12:48."

    "{i}Shit!{/i}"
    "You jump up from your chair and run to the kitchen aisle."
    "It's fine, you tell yourself as you throw the tea bag into the trash bin."
    "It's surely fine, you tell yourself as you carefully take a sip from the cup."
    pause (0.5)
    "It's not fine."
    "You spit the tea back."
    "It's bitter."
    "You pour the cup's content down the drain."
    "Well, no problem, you'll try again."
    "You check the clock."
    "12:53."
    "Okay, this time you're only do {i}one more{/i} task and then you'll check the tea."
    pause (2)
    "Okay, done!"
    "13:14."

    "{i}Fuck!{/i}"
    "How does this keep happening?!"
    "You don't even try tasting the tea this time, it goes straight into the sink."
    "One."
    "Last."
    "Time."
    "You set yourself a timer this time. No way you could miss it."
    "Your timer goes off and you swipe it away on your phone."
    "You'll just finish this sentence and then you're ready to go."
    "Done."
    "Finally, some good fucking tea."
    "You take a sip."
    pause (2)
    "{i}Eugh...{/i}"
    "This {i}feels{/i} unfair."
    "You think about smashing the cup briefly but you decide not to."
    "It's your favourite one and you won't get another one like it."
    "You could smash a plate."
    "But you would have to clean up afterwards and the effort wouldn't feel worth doing it."
    "You open a can of soda."
    "It's 7 pm again."


label ink:

    scene bg apartment day ch2
    with fade

    """
    Sometimes a shower does wonders in washing away any bad thoughts and pain.

    If you take a shower like a normal person, that is.

    You insist on turning the heat up so high that you come out of it looking like a lobster.

    It still feels nice, you guess. The heat.

    At this point, you're used to feeling cold.

    Maybe you really are already dead and your ghost spends time piloting your body around?

    You turn the heat up a little more.

    You let out a scream of surprise from the scalding water hitting your back but the feeling is gone after a few seconds.

    Your feet are still awfully cold and the water feels boiling hot on your toes.

    You grab the shower head and bend down to rinse your legs.

    You suddenly feel something dripping down your face.

    You use your free hand to wipe it away.

    Your fingers come back stained.

    You drop the shower head in shock.

    It hits the floor with a deafening bang, turns around, and hot water hits your eyes, momentarily blinding you.

    You shield yourself from the spray of water and look down.

    There is something black dripping from your face, you feel it run down your chin.

    You feel yourself hyperventilate, your vision growing foggy.

    No.

    {i}No!{/i}
    """

    scene black

    play music "audio/white_noise_edit.mp3" fadein 1.0

    """

    You squeeze your eyes shut and count to ten.

    1

    2

    3

    4

    5

    6

    7

    8

    9

    10.
    """
    stop music fadeout 3.0
    """
    You open your eyes again.

    The black is gone.

    Your nose is bleeding.

    """

label hospital:

    scene bg laptop 20
    with fade

    """
    You've been feeling off for the last couple weeks.

    Tired, sick, ill.

    It's probably the stress.

    It's always the stress.

    At least you think that way until late July.
    """

    scene bg apartment day ch2


    """
    Until you excuse yourself from the lecture hallway through to lie in your bed until the painkillers start to work.

    They don't.
    """
    show noise:
        alpha (0.3)
    scene red
    """
    Instead there's a sharp pain in your left side, like somebody's stabbed you.

    You've never been stabbed but you imagine that it would feel that way.

    It feels like somebody reached into your abdomen and has gripped your guts and is crushing, twisting them.

    You scream.

    Your camera isn't on.

    Your microphone isn't either, thankfully.

    You cry.
    """

    scene black
    with fade

    """
    You wake up an hour later, the lecture over, the voice call .

    You must've passed out from the pain or the painkillers finally kicked in.

    You grab your phone with shaky hands to make a call.
    """

    hide noise

    scene bg apartment day ch2
    with fade

    """
    Once you tell the nurse on the phone what happened, she tells you that she's calling an ambulance to get you.

    You said that you technically weren't allowed to leave the apartment complex.

    She said that they'll have to let an ambulance through, no matter the situation.

    This was the sixth hospital visit this year.
    """

    scene black
    with fade

    """
    You're at the doctors office the next day, only to get an emergency appointment to the gynaecologist.

    For a couple moments you panic.

    Were you having a miscarriage?

    Wait, not.

    You're a virgin.

    It's unlikely that you're the next Virgin Mary.

    {i}Unlikely{/i}, not impossible.

    You shake the thought off on the way to the doc.

    You repeat yourself for the third time.

    He suggests an ultrasound.
    """

    pause (1)

    "There are a couple things you never wanted to hear from a doctor."

    "One of them is {i}Oh, wow! Look at that!{/i}"

    "\"Well then\", he said, \"Looks like we found the culprit.\""

    "You just look at him."

    "You can't make anything out on the screenshot he's pointing at."

    "\"It's a cyst\", he explains, \"quite a large one. I'll call up the hospital right away.\""

    "You nod."

    "\"I'll have to take a blood sample, would that be okay?\""

    "You nod again."

    "\"Okay, then. Please wait in the room next door for a moment then.\""

    "You've done this a hundred times. You know the drill."

    "The nurse looks nervous. She's new."

    "She gathers the supplies when your vision suddenly darkens."

    "Your ears ring."

    t "Uhm, excuse me-?"

    "\"Yes?\", the nurse asks."

    t "I feel a little weird."

    "\"Oh, some people do when we take blood. That's normal.\""


    "\"No\", you hear yourself say from far away, \"I think I'm going to faint.\""

    "The nurse looks panicked. She's calling for someone."

    "You're sweating."

    "Someone helps you get up."

    "You see your feet move."

    "Step by step."

    "Your knee hits something."

    "\"Lie down\", a voice says."

    "You feel your body shift, something hard underneath your back."

    "Your vision clears."

    "The ringing stops."

    "\"Well, isn't that something new?\", you see the other nurse say to the new one before she turns to you."

    "\"Never had a patient faint before we did anything.\""

    "You smile weakly."

    "You feel like you're going to vomit."

label memento:

    play music "audio/darker_you.mp3" fadein 2.0

    scene bg therapist green

    psy "Have you ever thought about ending your life, Thea?"
    t "..."
    t "Yes."
    psy "How often?"
    t "Sometimes."
    v "Liar."

    psy "And when you think about ending your life, what do you think about?"
    psy "Do you think about how you are taking your life, do you think about your funeral?"

    "Don't."
    "Say."
    "It."

    t "I think about how everything will be when I'm gone."
    psy "And what is it like?"

    v "Don't cry."
    "Don't cry."
    "DON'T."
    "CRY."

    t "Better."
    psy "Better? How?"
    t "Everyone-"

    show noise:
        alpha (0.1)

    "You gulp down the lump in your throat."
    "It's getting hard to breathe."
    "Stay calm."

    t "Everyone is ... happier, I think."

    show noise:
        alpha (0.15)

    t "Their life is easier without me, they don't have to worry about me, they don't have to care about me. People would be happier if I was gone."
    psy "And why do think that?"

    show noise:
        alpha (0.2)

    "Breathe."

    t "Because I am a burden."
    show black:
        alpha (0.1)
    t "Because I am useless."
    show black:
        alpha (0.15)
    t "Because I'm a hindrance to others."
    show black:
        alpha (0.2)
    t "Because I am a waste of space."
    show black:
        alpha (0.25)
    t "Because-"

    "Your voice breaks."
    "Your cheeks feel hot and wet."

    v "Pathetic."
    v "Disgusting."

    psy "Calm down, Thea."

    "You try to stop crying but the tears won't stop coming."
    "You look up."
    show noise:
        alpha (0.25)
    show black:
        alpha (0.5)

    "You can't see anything clearly."

    psy "Stop crying, Thea."

    "You wipe the tears away with your sleeve."
    "New ones come to take their place."

    t "Sorry, I-"
    psy "I think we'll end the session here."
    psy "Crying won't get us anywhere."

    "She gets up and places the clipboard on her desk."

    psy "If you don't mind, I'll have to prepare everything for the next client."

    "You try to say something, but it comes out burbled."
    "It might've been \"Sorry\" or \"Okay\", you don't remember."

    scene black
    hide noise

    "You don't cry on the way home."
    "The next time you'll open up to somebody will be on the autopsy bed."
    stop music fadeout 3.0
    show screen tear(10, 0.2, 0.2, 0, 40)

label news:
    hide screen tear

    scene bg mailbox

    """
    You went downstairs for the first time in three weeks to check your mailbox.

    There's no point in going downstairs.

    It's not like you spend much time in the communal areas there and food boxes are delivered every week to your doorstep.

    {i}Were{/i} delivered every week.

    You open your mailbox and reach inside.

    You pull out a few pamphlets that talk about The Fog.

    They're full of numbers and words you've never heard before.

    You don't know what they mean.

    You don't really care what they mean.

    There's so many of them.

    You blink and they change, so there is no point in learning about them.

    There's something about rising contamination levels.

    You don't know what is being contaminated, or how.
    """

    steph "Thea!"
    show steph sigh day at right
    show thea neutral day at left
    t "Steph."
    steph "God, I've been looking for you."
    hide thea neutral day
    show thea uncomfy day at left

    "The one time youe leave your apartment to check your mail is the time someone goes looking for you."
    "Great."

    steph "No, it's okay."
    hide steph sigh day
    show steph worried day at right
    hide thea uncomfy day
    show thea neutral day at left
    steph "I've asked around, about the Newmans Yard stuff."
    t "Oh?"
    steph "Yeah..."
    hide steph worried day
    show steph sad day at right
    steph "So, apparently they found our Scavengers."
    steph "But because it's not, like, {i}an official{/i} thing, they've arrested them for breaking curfew."
    t "Oh, but they're okay then?"
    hide steph sad day
    show steph sigh day at right
    steph "If I knew..."
    steph "I've tried contacting them again, but I still got no response."
    t "Well, there must be a way to contact them, though."
    steph "But how? We're stuck here."
    t "I'm sure we got spare hazmat suits left."
    t "And if not, it's not like we can't ask to make a call."
    t "We'll have to ask for supplies anyway."
    t "Steph, they can't let us starve in here, if they're not planning on evacuating us."
    hide steph sigh day
    show steph worried day at right
    steph "Hm, so it's either they let us leave or they provide for us, now that the Scavengers are gone."
    t " I think so."

    "Steph looks deeply in thought for a moment. You're glad your little rant didn't seem to outlandish."

    hide thea neutral day
    show thea uncomfy day at left
    t "So..."
    t "What do you think?"
    steph "Well, I mean, it's not like there's anything stopping us from sending out another squad, right?"
    steph "Not scavengers, just someone to contact them?"
    t "I guess not..."

    hide steph worried day
    show steph neutral day at right
    steph "Well then!"
    steph "I'll see if I can figure something out."
    "Steph claps her hands once. She's looking rather confident in whatever plan she has."
    hide steph neutral day
    show steph happy day at right
    steph "Thanks, Thea!"
    t "D-don't mention it."
    steph "I'll keep in touch!"
    "She says and waves goodbye."

    hide steph neutral day
    "She's always gone so quickly."
    "She probably doesn't like talking to you all that much."
    jump faint
