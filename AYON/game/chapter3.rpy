﻿# The script of the game goes in this file.
$ vtext = spooktext(15)

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define l = Character("Lucas")
define kyle = Character("Kyle", what_font="YesYesNo.ttf")
define cop = Character("Policeman")
define g = Character("Guy")


# Declare images here. Start with Backgrounds and make seperate colums for sprites.

image bg mensa red = "images/bg/mensa_red.png"


#### Declare sprites here.
image lucas neutral day = "images/sprites/lucas/lucas_neutral_day.png"




# Declare sounds here.

define audio.siren_warning = "audio/siren_warning.mp3"


# The game starts here.

label faint:
    ###cut due to time contraints
    jump precognition

label precognition:

    scene black


    "You remember your mother telling you about the \"Swine's Eye\"."
    """
    It basically meant that someone who has the swine's eye is able to jinx things or people just by saying them out loud.

    It was a joke. Folklore.

    But as time went along, it stopped being funny.

    You started putting put pieces together.

    You remember your mother telling you a story from when you were little. She's awfully nonchalant about it, but it still creeps you out to this day.

    She told you, when you were old enough, that you were supposed to have a little brother.

    About four years after you were born, your mother got pregnant again. Another little miracle.

    She went to a check up in the hospital with you and your dad.

    She told you that you were looking very intense the whole time, well as intense as a four year old could be.
    """

    "When she asked you what were wrong, you touched her belly and said, in full seriousness \"Mommy, the baby is gone.\""

    """
    Your parents thought of it as a silly thing you said. A four year old wouldn't understand how birthing worked.

    And then, later the same day, the doctors told your mother that the baby would end up stillborn.

    You convinced yourself that you somehow jinxed it.

    You were a curse.

    You were a demon, someone who brought plagues upon the lands, making men infertile and pregnant women give born to stillborn children.

    You killed that baby.

    You forgot that story until the start of this year.

    You had a thought.

    Well, that was, in itself, not an unusual occurrence.

    Sometimes you would get these weird thoughts that suddenly popped into your head out of nowhere.

    On your phone you wrote:
    """

    "\"It began with a 58 year old man stabbing his wife at the train station and then came the apocalypse.\""

    """
    Your father had told you earlier that day that a former work colleague of his shot his wife at the train station for leaving him the week prior.

    The Fog came the week after you made that note.
    """

label light:

    scene bg apartment day ch2
    with fade

    "In may, there was a sliver of light on the horizon that turned out to be as bright as another sun."
    "Perhaps this is grossly exaggerated."
    "But it did feel like a rescue then a friend messaged you out of the blue one day."

    kyle "Hey, would you like to join our roleplaying group?"
    kyle "One of our players left and we're looking for someone to jump in"
    kyle "I thought that maybe you're interested?"

    "You were ecstatic at first."
    "You've never played yourself, you have been the dedicated \"Forever GM\" of your own group for four years."
    "And you have never played Oubliette and Lizards, it could take your mind off of {i}this{/i} entire situation."
    "You reply quickly."

    t "Sure!"
    t "Who else is in that group?"
    kyle "Oh well..."

    "He names a bunch of people from the other major."
    "You know them, they're all very nice from what you're seen."
    "Still..."
    "They're starngers in a way."
    "Everything else is arranged quickly."
    "You still have the Game Master's phone number somewhere from the one time you forgot your sketchbook in class, and he's free the same evening to talk you through everything."
    "It feels weird joining an existing group."
    "But it's always been like this."
    "You were always the one joining existing friend groups, so you always were the fifth wheel."
    "That was okay."
    "The spare tire is just as important as the rest!"
    "But in all honesty, do you regularly check in on the fifth wheel?"
    "Tuns out you're supposed to do that."
    "You learned that when you were making your driver's licence against your will."
    "Your parents thought it was a good idea to put an emotionally unstable teenager into a two-ton-murder-machine."

    pause (0.5)
    "Speaking of unstable teens turning into murder machines:"
    "After about an hour of convincing, that the group did - in fact - not need another Rogue, you let the GM convince you of playing a Warlock."
    "Selling your soul for a miniscule amount of magic seemed like a good deal."
    "But you weren't particllary known for making good decisons."
    "Your Warlock turned out to be an overly traumatized nineteen-year old girl who just so happened to gain mass-murdering magic abilities."
    "You know, the usual."
    "You were worried that you spent 4 hours for nothing, you weren't a full-fledged memeber yet, you were there for a \"test session\" to see in you mesh well with the group."
    "And to your, and maybe the DM's surprise, everything went better than expected."
    "The group was happy to have you and the characters seemed to immediately like your Warlock."
    "Well, who didn't love a mentally unstable, highly anxious, traumatized girl with a tragic backstory?"
    pause (0.2)
    "{i}Anyway.{/i}"
    "You never remembered having this much fun in your own sessions."
    "Which meant that your player's probably weren't having a lot of fun, as well."
    "A reason to consider disbanding it."
    "You somehow managed to play almost every two weeks for a couple months."
    "Over the span of six months, you had almost as many sessions as your old group had in four years."
    "And then came a break."
    "The great hiatus, you called it."
    "And it was only then you noticed how much you depended on these 4 to 6 hours sessions."
    "You have projected fears onto that character and it made you feel good being comforted by others even if it was fictitious."
    "You used this to emotionally exploit these people you wanted to call friends, just to hear \"You matter to us!\" from someone."
    v "You're disgusting."
    jump nerve

label nerve:

    scene bg apartment day ch2
    with fade

    play sound "audio/knock.mp3"

    "It's about nine in the morning when you're woken up by a harsh knock on your door."
    steph "The police is downstairs. They want everyone to come into the lounge."
    t "On my way!"

    "You hear her footsteps and get dressed quickly before you make your way down the stairs."
    "It was quicker than the elevator. Maybe."
    "You open the door to the lounge. The handle is somewhere down to your knees, a design decision you never got behind and push."

    scene bg hallway
    with fade

    play music "audio/crowd_talking.mp3"

    "The room is full with people."
    "It reminds you of the time you used to go to conventions."
    "You haven't been to one in six years."
    "You shake the thought off and search the crowd for a familiar face."
    "You can't see Steph, but you do see Lucas standing at one of the tables, chatting with someone you don't recognize."
    "Lucas was one of your other neighbours. You met him when you had just moved in for a week and you spent an hour outside due to a fire alarm."
    "Ugh."
    "You don't want to interrupt."
    "You probably look awful too, you just woke up after all."
    "You startle unintentionally when Lucas suddenly turns around and meets your gaze."
    "He smiles and waves to you."
    "You slowly walk towards him."

    show lucas neutral day at left
    show thea uncomfy day at right
    l "Morning!"

    "How the hell is he in a mood this good this early in the morning?"

    hide thea uncomfy day
    show thea blush day at right
    t "Uh, Steph called me, well, knocked on my door."
    t "Why are we here?"
    "Lucas looks ahead to somewhere in the crowd and nods in that direction. You can't quite see what he's referring to."
    l "Police's here. They have an announcement or something."
    "Probably about the Scavengers."
    hide thea blush day
    show thea neutral day at right
    t "Did they say what it's about?"
    l "Nah, nothing."

    "A sudden clap, followed by a \"Okay, everyone\" echoed through the room, silencing the chatter."
    stop music fadeout 1.0

    cop "I assume this is going to be everybody."
    cop "Alright, I'll try to make this short."

    "Well, it's not like you have somewhere to be."

    cop "As some of you might have heard, Newmans Yard was recently put into quarantine as well, because the contamination levels have risen above the threshold."
    cop "And we arrested a group of trespassers that claimed to be from your apartment complex."
    cop "They also claimed that they were looking for supplies and groceries. Something we call looting, something that is illegal."
    g " It is {i}not{/i} looting!"
    "Someone from the crowd shouts."
    g "We're {i}stuck{/i} in here, someone had to go out and get us supplies!"

    play music "audio/crowd_talking.mp3"
    "The crowd is getting restless."

    cop "Silence, SILENCE!"
    cop "Those goods weren't sold to your little group, they were {i}stolen{/i}, so that point is moot."
    g "And what are our alternatives? Starve?!"
    cop "Please let me finish, young man."
    stop music fadeout 1.0
    cop "We have taken the group into custody and confiscated the good they acquired."

    play music "audio/crowd_talking.mp3" fadein 1.0

    cop "Still, we have recognized that your current situation isn't optimal, so we tried contacting the local administration to ensure some kind of provision of groceries and necessities."
    cop "However, as you can imagine this will take some time."
    cop "That's why we have decided to temporarily lift the quarantine on this building."
    cop "Any of you will be allowed to leave the building for necessary trips with a permit."
    cop "You will have to go through a screening process to get said permit, we have to make sure that you don't accidentally expose yourself to more of the contaminants than necessary."

    jump family







    return
